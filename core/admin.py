from django.contrib import admin
from django.shortcuts import redirect
from django.http import HttpResponse
from tablib import Dataset
from django.urls import path
import csv
import xlwt
from .models import ManageCsv


# Register your models here.

class ExportCsvMixin:
    """
    Export CSV/XLS Mixin
    """

    def export_as_csv(self, request, queryset):
        """
        generic export csv method
        :param request:
        :param queryset:
        :return:
        """
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(
            meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow(
                [getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = "Export selected csv"

    def export_as_xls(self, request, queryset):
        """
        generic export xls method
        :param request:
        :param queryset:
        :return:
        """

        meta = self.model._meta
        response = HttpResponse(content_type='application/vnd.ms-excel')

        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(
            meta)

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet("sheet-1")

        row_num = 0

        columns = [(field.name, 10000) for field in meta.fields]

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num][0], font_style)
            # set column width
            ws.col(col_num).width = columns[col_num][1]

        font_style = xlwt.XFStyle()
        font_style.alignment.wrap = 1

        for obj in queryset:
            row_num += 1

            [ws.write(row_num, col_num, getattr(obj, field[0]).__str__(),
                      font_style)
             for col_num, field in enumerate(columns)]

        wb.save(response)
        return response

    export_as_xls.short_description = "Export selected xls"

    def download_csv_template(self, field_names):
        """
        generic method to download csv template
        :param list field_names: list of field names
        :return:
        """
        meta = self.model._meta

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(
            meta)
        writer = csv.writer(response)

        writer.writerow(field_names)

        return response

    download_csv_template.short_description = "Download CSV Template "

    def download_xls_template(self, field_names):
        """
        download empty xls template wit headers
        :param field_names: list of fields names
        :return:
        """

        meta = self.model._meta
        response = HttpResponse(content_type='application/vnd.ms-excel')

        response['Content-Disposition'] = 'attachment; filename={}.xls'.format(
            meta)

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet("sheet-1")

        row_num = 0

        columns = [(field_name, 10000) for field_name in field_names]

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num][0], font_style)
            # set column width
            ws.col(col_num).width = columns[col_num][1]

        font_style = xlwt.XFStyle()
        font_style.alignment.wrap = 1

        wb.save(response)
        return response


class ManageCsvAdmin(admin.ModelAdmin, ExportCsvMixin):
    change_list_template = "admin/core/managecsv.html"

    def get_urls(self):
        """
        Returns the additional urls used by the Reversion admin.
        :return: List of urls
        """
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):
        """
        method to import the csv/xls into ReasonCodeMapping model
        :param request:
        :return:
        """
        if request.method == "POST":
            file = request.FILES.get("file")
            if not file:
                self.message_user(request, "no file selected")
                return redirect("..")

            dataset = Dataset()
            if file.content_type == "text/csv":
                imported_data = dataset.load(file.read().decode('utf-8'))
            else:
                imported_data = dataset.load(file.read())

            reader = imported_data.dict

            try:
                for row in reader:
                    obj, created = ManageCsv.objects.get_or_create(**row)
                message = "Your csv file has been imported"
            except Exception as e:
                message = "error: {}".format(e)

            self.message_user(request, message)
            return redirect("..")


admin.site.register(ManageCsv, ManageCsvAdmin)
