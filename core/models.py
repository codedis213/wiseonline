from django.db import models


# Create your models here.

class ManageCsv(models.Model):
    uniq_id = models.CharField(max_length=256, null=True, blank=True)
    crawl_timestamp = models.CharField(max_length=256, null=True, blank=True)
    product_url = models.CharField(max_length=256, null=True, blank=True)
    product_name = models.CharField(max_length=256, null=True, blank=True)
    product_category_tree = models.CharField(max_length=256, null=True,
                                             blank=True)
    pid = models.CharField(max_length=256, null=True, blank=True)
    retail_price = models.CharField(max_length=256, null=True, blank=True)
    discounted_price = models.CharField(max_length=256, null=True, blank=True)
    image = models.CharField(max_length=256, null=True, blank=True)
    is_FK_Advantage_product = models.CharField(max_length=256, null=True,
                                               blank=True)
    description = models.CharField(max_length=256, null=True, blank=True)
    product_rating = models.CharField(max_length=256, null=True, blank=True)
    overall_rating = models.CharField(max_length=256, null=True, blank=True)
    brand = models.CharField(max_length=256, null=True, blank=True)
    product_specifications = models.CharField(max_length=256, null=True,
                                              blank=True)
