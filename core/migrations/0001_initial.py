# Generated by Django 2.2.5 on 2019-09-13 05:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ManageCsv',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uniq_id', models.CharField(blank=True, max_length=256, null=True)),
                ('crawl_timestamp', models.CharField(blank=True, max_length=256, null=True)),
                ('product_url', models.CharField(blank=True, max_length=256, null=True)),
                ('product_name', models.CharField(blank=True, max_length=256, null=True)),
                ('product_category_tree', models.CharField(blank=True, max_length=256, null=True)),
                ('pid', models.CharField(blank=True, max_length=256, null=True)),
                ('retail_price', models.CharField(blank=True, max_length=256, null=True)),
                ('discounted_price', models.CharField(blank=True, max_length=256, null=True)),
                ('image', models.CharField(blank=True, max_length=256, null=True)),
                ('is_FK_Advantage_product', models.CharField(blank=True, max_length=256, null=True)),
                ('description', models.CharField(blank=True, max_length=256, null=True)),
                ('product_rating', models.CharField(blank=True, max_length=256, null=True)),
                ('overall_rating', models.CharField(blank=True, max_length=256, null=True)),
                ('brand', models.CharField(blank=True, max_length=256, null=True)),
                ('product_specifications', models.CharField(blank=True, max_length=256, null=True)),
            ],
        ),
    ]
